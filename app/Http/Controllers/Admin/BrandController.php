<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Http\Requests\AddBrandRequest;
use App\Http\Requests\EditBrandRequest;
class BrandController extends Controller
{
   public function getBrand()
   {
   	$data['brands'] = Brand::paginate(5);
   	return view('backend.brand.list',$data);
   }

   public function getAddBrand()
   {
   	return view('backend.brand.add');
   }

   public function postAddBrand(AddBrandRequest $request)
   {
   		
   		$fileName = $request->file->getClientOriginalName();
   		$brand = new Brand;
   		$brand->name = $request->name;
   		$brand->slug = str_slug($request->name);
   		$brand->logo = $fileName;
   		$brand->description = $request->description;
   		$brand->status = $request->status;
   		$brand->save();
   		$request->file->storeAs('logo',$fileName);
   		return redirect()->intended('admin/brand');
   }

   public function getEditBrand($id)
   {
   		$data['brands'] = Brand::find($id);
   		return view('backend.brand.edit',$data);
   }

   public function postEditBrand(EditBrandRequest $request,$id)
   {
   		$brand = new Brand();
    	$arr['name'] = $request->name;
    	$arr['slug'] = str_slug($request->name);
    	$arr['description'] = $request->description;
    	$arr['status'] = $request->status;
    	if($request->hasFile('file')){
    		$img = $request->file->getClientOriginalName();
    		$arr['logo'] = $img;
    		$request->file->storeAs('logo',$img);
    	}
    	$brand::where('id',$id)->update($arr);
    	return redirect()->intended('admin/brand');
   }

   public function getDeleteBrand($id)
   {
   		Brand::destroy($id);
   		return redirect()->intended('admin/brand');
   }
}
