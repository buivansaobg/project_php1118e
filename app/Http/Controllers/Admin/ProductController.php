<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Brand;
use App\Http\Requests\AddProductRequest;
use App\Http\Requests\EditProductRequest;
class ProductController extends Controller
{
    public function getProduct()
    {
    	$data['products'] = Product::paginate(5);
    	return view('backend.product.list',$data);
    }

    public function getAddProduct()
    {
    	$data['brands'] = Brand::all();
    	return view('backend.product.add',$data);
    }

    public function postAddProduct(AddProductRequest $req)
    {

    	$fileName = $req->file->getClientOriginalName();
    	$product = new Product;
    	$product->name = $req->name;
    	$product->slug = str_slug($req->name);
    	$product->image = $fileName;
    	$product->description = $req->brand;
    	$product->brand_id = $req->brand;
    	$product->stock_price = $req->stockPrice;
    	$product->price = $req->price;
    	$product->content = $req->content;
    	$product->noibat = $req->noibat;
    	$product->news = $req->news;
    	$product->meta_title = $req->metaTitle;
    	$product->meta_description = $req->metaDescription;
    	$product->meta_keyword = $req->metaKeyword;
    	$product->stock = $req->stock;
    	$product->status = $req->status;
    	$product->save();
    	$req->file->storeAs('products',$fileName);
    	return redirect()->intended('admin/product');

    }

    public function getEditProduct($id)
    {
    	$data['brands'] = Brand::all();
    	$data['product'] = Product::find($id);
    	return view('backend.product.edit',$data);
    }

    public function postEditProduct(EditProductRequest $req,$id)
    {
    	$brand = new Product();
    	$arr['name'] = $req->name;
    	$arr['slug'] = str_slug($req->name);
    	$arr['description'] = $req->description;
    	$arr['brand_id'] = $req->brand;
    	$arr['price'] = $req->price;
    	$arr['content'] = $req->content;
    	$arr['noibat'] = $req->noibat;
    	$arr['news'] = $req->news;
    	$arr['meta_title'] = $req->metaTitle;
    	$arr['meta_description'] = $req->metaDescription;
    	$arr['meta_keyword'] = $req->metaKeyword;
    	$arr['stock'] = $req->stock;
    	$arr['status'] = $req->status;
    	if($req->hasFile('file')){
    		$img = $req->file->getClientOriginalName();
    		$arr['image'] = $img;
    		$req->file->storeAs('products',$img);
    	}
    	$brand::where('id',$id)->update($arr);
    	return redirect()->intended('admin/product');
    }

    public function getDeleteProduct($id)
    {
    	Product::destroy($id);
    	return redirect()->intended('admin/product');
    }
}
