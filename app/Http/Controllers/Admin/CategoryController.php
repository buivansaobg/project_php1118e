<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Http\Requests\AddCateRequest;
use App\Http\Requests\EditCateRequest;
class CategoryController extends Controller
{
    public function getCate()
    {
    	$data['categories'] = Category::paginate(5);
    	return view('backend.category.list',$data);
    }

    public function getAddCate()
    {
    	$data['categories'] = Category::all();
    	return view('backend.category.add',$data);
    }

    public function postAddCate(AddCateRequest $request)
    {
    	$category = new Category;
    	$category->name = $request->name;
    	$category->slug = str_slug($request->name);
    	$category->description = $request->description;
    	$category->parent_id = $request->parent;
    	$category->content = $request->content;
    	$category->position = $request->position;
    	$category->meta_title = $request->metaTitle;
    	$category->meta_keyword = $request->metaKeyword;
    	$category->meta_description = $request->metaDescription;
    	$category->status = $request->status;

    	$category->save();
    	return redirect()->intended('admin/category');
    }

    public function getEditCate($id)
    {
    	$data['cate'] = Category::find($id);
    	$data['categories'] = Category::all();
    	return view('backend.category.edit',$data);
    }

    public function postEditCate(EditCateRequest $request,$id)
    {
    	$category = Category::find($id);
    	$category->name = $request->name;
    	$category->slug = str_slug($request->name);
    	$category->description = $request->description;
    	$category->parent_id = $request->parent;
    	$category->content = $request->content;
    	$category->position = $request->position;
    	$category->meta_title = $request->metaTitle;
    	$category->meta_keyword = $request->metaKeyword;
    	$category->meta_description = $request->metaDescription;
    	$category->status = $request->status;
    	$category->save();
    	return redirect()->intended('admin/category');
    }

    public function getDeleteCate($id)
    {
    	Category::destroy($id);
    	return back();
    }
}
