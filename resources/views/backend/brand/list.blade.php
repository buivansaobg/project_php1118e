@extends('backend.layout.index')
@section('title','Danh sách thương hiệu')
@section('main')
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Thương hiệu</h1>
		</div>
	</div><!--/.row-->

	<div class="row">
		<div class="col-xs-12 col-md-12 col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Danh sách Thương hiêuj</div>
				<div class="panel-body">
					<div class="bootstrap-table">
						<table class="table table-bordered">
							<thead>
								<tr class="bg-primary">
									<th>Tên thương hiệuc</th>
									<th>Hình</th>
									<th>Trạng thái</th>
									<th style="width:30%">Tùy chọn</th>
								</tr>
							</thead>
							<tbody>
								@foreach($brands as $brand)
								<tr>
									<td>{{$brand->name}}</td>
									<td>
										<img src="{{asset('storage/app/logo/' . $brand->logo)}}" height="100px">
									</td>
									<td>
										@if($brand->status == 1)
										{{"Hiển thị"}}
										@else 
										{{"không hiển thị"}}
										@endif
									</td>
									

									<td>
										<a href="{{asset('admin/brand/edit/' . $brand->id)}}" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Sửa</a>
										<a href="{{asset('admin/brand/delete/' . $brand->id)}}" onclick="return confirm('Bạn có chắc chắn muốn xóa?')" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Xóa</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="clearfix"></div>
					<div class="row">
						{{$brands->links()}}
					</div>
				</div>
			</div>
		</div>
	</div><!--/.row-->
</div>	<!--/.main-->
@stop