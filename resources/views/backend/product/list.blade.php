@extends('backend.layout.index')
@section('title','Danh sách sản phẩm')
@section('main')
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Danh sách sản phẩm</h1>
		</div>
	</div><!--/.row-->

	<div class="row">
		<div class="col-xs-12 col-md-12 col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Danh sách sản phẩm</div>
				<div class="panel-body">
					<div class="bootstrap-table">
						<table class="table table-bordered">
							<thead>
								<tr class="bg-primary">
									<th>Tên sản phẩm</th>
									<th>Slug</th>
									<th>Hình</th>
									<th>Mô tả</th>
									<th>Thương hiệu</th>
									<th>Giá</th>
									<th>Giảm giá</th>
									<th>Nội dung</th>
									<th>Nổi bật</th>
									<th>Sản phẩm mới</th>
									<th>trạng thái</th>

									<th style="width:30%">Tùy chọn</th>
								</tr>
							</thead>
							<tbody>
								@foreach($products as $product)
								<tr>
									<td>{{$product->name}}</td>
									<td>{{$product->slug}}</td>
									<td>
										
									</td>
									<td>{{$product->description}}</td>
									<td>{{$product->brand->name}}</td>
									<td>{{$product->price}}</td>
									<td>{{$product->stock_price}}</td>
									<td>{!! $product->content !!}</td>
									<td>
										@if($product->Noibat == 1)
										{{"Nổi Bật"}}
										@else 
										{{"Không nổi bật"}}
										@endif
									</td>
									<td>
										@if($product->New == 1)
										{{"Sản phẩm mới"}}
										@else
										{{"Sản phẩm cũ"}}
										@endif
									</td>
									<td>
										@if($product->status == 1)
										{{"Hiện"}}
										@else 
										{{"Ẩn"}}
										@endif
									</td>
									
									<td>
										<a href="{{asset('admin/product/edit/' . $product->id)}}" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Sửa</a>
										<a href="{{asset('admin/product/delete/' . $product->id)}}" onclick="return confirm('Bạn có chắc chắn muốn xóa?')" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Xóa</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="clearfix"></div>
					<div class="row">
						{{$products->links()}}
					</div>
				</div>
			</div>
		</div>
	</div><!--/.row-->
</div>	<!--/.main-->
@stop