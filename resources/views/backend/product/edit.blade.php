@extends('backend.layout.index')
@section('title','Thêm sản phẩm')
@section('main')
<div class="col-xs-12 col-md-10 col-lg-10 pull-right">
	<div class="panel panel-primary">
		<div class="panel-heading">
			Thêm Sản phẩm
		</div>
		<div class="panel-body">
			@include('errors.note')
			<form  method="POST" accept-charset="utf-8" enctype="multipart/form-data">
				{{csrf_field()}}
				<div class="form-group col-xs-12 col-lg-12">
					<label>Tên sản phẩm:</label>
					<input type="text" name="name" class="form-control" placeholder="Tên sản phẩm..." value="{{$product->name}}">
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Description:</label>
					<input type="text" name="description" class="form-control" placeholder="Mô tả" value="{{$product->description}}">
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label for="">Hình</label>
					<input type="file" name="file" class="form-control">
					<img src="{{asset('storage/app/products/' . $product->image)}}" height="100px">
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Thương hiệu:</label>
					<select name="brand" class="form-control">
						@foreach($brands as $brand)
						<option 
							@if($product->brand_id == $brand->id)
							{{"selected"}}
							@endif
						value="{{$brand->id}}">{{$brand->name}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Giá:</label>
					<input type="text" name="price" class="form-control" placeholder="1000000" value="{{$product->price}}">
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>giảm giá:</label>
					<input type="text" name="stockPrice" class="form-control" placeholder="1000000" value="{{$product->stock_price}}">
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Content:</label>
					<textarea name="content" class="ckeditor">{{$product->content}}</textarea>
					<script type="text/javascript">
						var editor = CKEDITOR.replace('content',{
							language:'vi',
							filebrowserImageBrowseUrl: '../../editor/ckfinder/ckfinder.html?Type=Images',
							filebrowserFlashBrowseUrl: '../../editor/ckfinder/ckfinder.html?Type=Flash',
							filebrowserImageUploadUrl: '../../editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
							filebrowserFlashUploadUrl: '../..editor//public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
						});
					</script>

				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Nổi bật:</label><br>
					<input 
						@if($product->noibat == 1)
						{{"checked"}}
						@endif
						type="radio" name="noibat" value="1">Nổi bật
					<input 
						@if($product->noibat == 0)
						{{"checked"}}
						@endif
						type="radio" name="noibat" value="0">Không nổi bật
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Sản phẩm mới:</label><br>
					<input 
						@if($product->news == 1)
						{{"checked"}}
						@endif
						type="radio" name="news" value="1">Sản phẩm mới
					<input 
						@if($product->news == 0)
						{{"checked"}}
						@endif
					 type="radio" name="news" value="0">Sản phẩm cũ
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Meta_title:</label>
					<textarea name="metaTitle" class="ckeditor">{{$product->meta_title}}</textarea>
					<script type="text/javascript">
						var editor = CKEDITOR.replace('metaTitle',{
							language:'vi',
							filebrowserImageBrowseUrl: '../../editor/ckfinder/ckfinder.html?Type=Images',
							filebrowserFlashBrowseUrl: '../../editor/ckfinder/ckfinder.html?Type=Flash',
							filebrowserImageUploadUrl: '../../editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
							filebrowserFlashUploadUrl: '../..editor//public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
						});
					</script>
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Meta_keyword:</label>
					<textarea name="metaKeyword" class="ckeditor">{{$product->meta_keyword}}</textarea>
					<script type="text/javascript">
						var editor = CKEDITOR.replace('metaKeyword',{
							language:'vi',
							filebrowserImageBrowseUrl: '../../editor/ckfinder/ckfinder.html?Type=Images',
							filebrowserFlashBrowseUrl: '../../editor/ckfinder/ckfinder.html?Type=Flash',
							filebrowserImageUploadUrl: '../../editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
							filebrowserFlashUploadUrl: '../..editor//public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
						});
					</script>
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Meta_description:</label>
					<textarea name="metaDescription" class="ckeditor">{{$product->meta_description}}</textarea>
					<script type="text/javascript">
						var editor = CKEDITOR.replace('metaDescription',{
							language:'vi',
							filebrowserImageBrowseUrl: '../../editor/ckfinder/ckfinder.html?Type=Images',
							filebrowserFlashBrowseUrl: '../../editor/ckfinder/ckfinder.html?Type=Flash',
							filebrowserImageUploadUrl: '../../editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
							filebrowserFlashUploadUrl: '../..editor//public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
						});
					</script>
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Tình trạng kho:</label><br>
					<input 
						@if($product->stock == 1)
						{{"checked"}}
						@endif
						type="radio" name="stock" value="1">Còn hàng
					<input 
						@if($product->stock == 0)
						{{"checked"}}
						@endif
					type="radio" name="stock" value="0">Hết hàng
				</div>

				<div class="form-group col-xs-12 col-lg-12">
					<label>Status:</label>
					<select name="status" class="form-control">
						<option 
						@if($product->status == 1)
						{{"selected"}}
						@endif
						value="1">Hiển thị</option>
						<option 
						@if($product->status == 0)
						{{"checked"}}
						@endif
						value="0">Không hiển thị</option>
					</select>
				</div>
				<div class="form-group">
					<input type="submit" name="submit" value="Update" class="btn btn-primary " >
				</div>
			</div>
		</form>
	</div>
</div>
@stop