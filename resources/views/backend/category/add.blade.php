@extends('backend.layout.index')
@section('title','Thêm sản phẩm')
@section('main')
<div class="col-xs-12 col-md-10 col-lg-10 pull-right">
	<div class="panel panel-primary">
		<div class="panel-heading">
			Thêm danh mục
		</div>
		<div class="panel-body">
			@include('errors.note')
			<form  method="POST" accept-charset="utf-8">
				{{csrf_field()}}
				<div class="form-group col-xs-12 col-lg-12">
					<label>Tên danh mục:</label>
					<input type="text" name="name" class="form-control" placeholder="Tên danh mục...">
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Description:</label>
					<input type="text" name="description" class="form-control" placeholder="Mô tả">
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Parent_id:</label>
					<select name="parent" class="form-control">
						@foreach($categories as $cate)
						<option value="{{$cate->id}}">{{$cate->name}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Content:</label>
					<textarea name="content" class="ckeditor"></textarea>
					<script type="text/javascript">
						var editor = CKEDITOR.replace('content',{
							language:'vi',
							filebrowserImageBrowseUrl: '../../editor/ckfinder/ckfinder.html?Type=Images',
							filebrowserFlashBrowseUrl: '../../editor/ckfinder/ckfinder.html?Type=Flash',
							filebrowserImageUploadUrl: '../../editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
							filebrowserFlashUploadUrl: '../..editor//public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
						});
					</script>

				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Position:</label>
					<input type="text" name="position" class="form-control">
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Meta_title:</label>
					<textarea name="metaTitle" class="ckeditor"></textarea>
					<script type="text/javascript">
						var editor = CKEDITOR.replace('metaTitle',{
							language:'vi',
							filebrowserImageBrowseUrl: '../../editor/ckfinder/ckfinder.html?Type=Images',
							filebrowserFlashBrowseUrl: '../../editor/ckfinder/ckfinder.html?Type=Flash',
							filebrowserImageUploadUrl: '../../editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
							filebrowserFlashUploadUrl: '../..editor//public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
						});
					</script>
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Meta_keyword:</label>
					<textarea name="metaKeyword" class="ckeditor"></textarea>
					<script type="text/javascript">
						var editor = CKEDITOR.replace('metaKeyword',{
							language:'vi',
							filebrowserImageBrowseUrl: '../../editor/ckfinder/ckfinder.html?Type=Images',
							filebrowserFlashBrowseUrl: '../../editor/ckfinder/ckfinder.html?Type=Flash',
							filebrowserImageUploadUrl: '../../editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
							filebrowserFlashUploadUrl: '../..editor//public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
						});
					</script>
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Meta_description:</label>
					<textarea name="metaDescription" class="ckeditor"></textarea>
					<script type="text/javascript">
						var editor = CKEDITOR.replace('metaDescription',{
							language:'vi',
							filebrowserImageBrowseUrl: '../../editor/ckfinder/ckfinder.html?Type=Images',
							filebrowserFlashBrowseUrl: '../../editor/ckfinder/ckfinder.html?Type=Flash',
							filebrowserImageUploadUrl: '../../editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
							filebrowserFlashUploadUrl: '../..editor//public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
						});
					</script>
				</div>
				<div class="form-group col-xs-12 col-lg-12">
					<label>Status:</label>
					<select name="status" class="form-control">
						<option value="1">Hiển thị</option>
						<option value="0">Không hiển thị</option>
					</select>
				</div>
				<div class="form-group">
					<input type="submit" name="submit" value="Thêm mới" class="btn btn-primary " >
				</div>
			</div>
		</form>
	</div>
</div>
@stop